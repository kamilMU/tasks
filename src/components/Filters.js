import React, {Component} from 'react';

const checkboxes = [
    { name: 'isUrgent', alias: 'Срочные' },
    { name: 'isNight', alias: 'Ночные' },
    { name: 'isPersonal', alias: 'Персональные' }
    ]

export default class Filters extends Component {
    constructor(props) {
        super(props);
        this.state={
            isUrgent: false,
            isNight: false,
            isPersonal: false
        }
    }
    handleFilterChange = async (e) => {        
        await this.setState({ [e.target.name]: e.target.checked })
        this.props.onFilterChange(this.state) 
    }
    render() {
        return (
        <div className="filters_">
            <input 
                onChange={(e) => this.props.onSearch(e.target.value)} 
                type="text" 
                className="input_filter" 
                placeholder='Поиск по слову'>
            </input>
            {checkboxes.map(checkbox => 
            <div key={checkbox.name}>
                <label className="checkbox"> 
                    <input type="checkbox" name={checkbox.name} onChange={this.handleFilterChange}/>
                    <span>{checkbox.alias}</span>
                </label>
            </div>) 
            }
            <div>
                <select className="main_select"> 
                    <option>User</option> 
                </select> 
            </div>
        </div>
        )
    }
}