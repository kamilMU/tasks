import React from 'react';
import { Switch, Route } from 'react-router-dom';
import NewTasks from '../components/main_content/NewTasks.js';
import Finance from '../components/main_content/Finance.js';
import Company from '../components/main_content/Company.js';
import Statistics from '../components/main_content/Stats.js';


const Main = () => (
  <main>
    <Switch>
      <Route  path='/tasks' exact component={NewTasks}/>
      <Route path='/finance' exact component={Finance}/>
      <Route path='/company' exact component={Company}/>
      <Route path='/stats' exact component={Statistics}/>
    </Switch>
  </main>
)

export default Main;