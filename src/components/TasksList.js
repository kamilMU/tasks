import React from 'react';
import TaskForm from '../components/TasksContent/TaskForm.js'
import Table from '../components/Table';
import Filters from '../components/Filters';

export default class TasksList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [
            ],
            filtredItems: [],
            clicked: false,
            text: ""
        }
    }
    async componentDidMount() {
        await fetch('../myJSON.json')
        .then(response => { 
            return response.json() 
        })
        .then(data => { 
            this.setState({ filtredItems: data }) 
        })
        .catch(err => {
            console.log("Error Reading data " + err);
        });
        this.setState({
            items: this.state.filtredItems
        })
    }
    toggleClick = () => {
        this.setState({
            clicked: !this.state.clicked
        })
    }
    
    addTask = (newTask) => {
        this.setState({ filtredItems: [...this.state.filtredItems, newTask] })
        this.setState({ clicked: false })
    }

    filterArrayWithCheckboxes = (filters) => {
        console.log(filters)
        const filterKeys = Object.keys(filters);
        const activeFilters = filterKeys.filter(key => filters[key]);
        const tasks = this.state.items;
        const filteredTasks = tasks.filter(task => {
            return activeFilters.every(key => task[key])
        })
        console.log(activeFilters, filteredTasks)
        this.setState({ filtredItems: filteredTasks })
    }

    filterArrayWithInput = (inputFilter) => {
        console.log(inputFilter)
        this.setState({ filtredItems: this.state.items.filter(function(item) { 
            return item.tittle.toString().toLowerCase().search(inputFilter.toLowerCase()) !== -1 
            }) 
        })
    }

    render() {
        return (
            <div className="tasks_under">
                <div>
                    <button 
                        className="button" 
                        onClick={this.toggleClick}>+</button> 
                    {this.state.clicked && 
                    <TaskForm 
                        onTaskAdd={this.addTask} 
                    />}
                </div>
                <Filters 
                    onFilterChange={this.filterArrayWithCheckboxes} 
                    onSearch={this.filterArrayWithInput} 
                    items={this.state.items} 
                />
                <Table filtredItems={this.state.filtredItems} 
                />
            </div>
        )
    }
}