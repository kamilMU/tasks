import React from 'react';

class TaskForm extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            ID: '',
            city: '',
            tittle: '',
            deadline: '',
            money: '',
            clicked: false
        }
    }
    handleAddTask = (e) => {
        e.preventDefault();
        const newTask = {
            id: Date.now(),
            ID: this.state.ID,
            city: this.state.city,
            tittle: this.state.tittle,
            deadline: this.state.deadline,
            money: this.state.money
        }
        if(this.state.tittle === '' || this.state.city==='') {
            alert('Нужно заполнить все поля')
        } else {
            this.props.onTaskAdd(newTask)
        }
        this.setState({ 
            ID: '',
            city: '',
            tittle: '',
            deadline: '',
            money: ''
        })
    }

    render() {
        return (
            <div className="taskForm">
                <div className="taskForm_Content">
                    <div className="taskForm_Content_spans">
                        <div><span>ID</span></div>
                        <div><span>Город</span></div>
                        <div><span>Название</span></div>
                        <div><span>Выполнить до</span></div>
                        <div><span>Сумма</span></div>
                    </div>
                    <div className="taskForm_Content_inputs">
                        <div>
                            <input value={this.state.ID} name="ID" onChange={(e) => this.setState({ [e.target.name]: e.target.value }, console.log(this.state.ID))} />
                        </div>
                        <div>
                            <input value={this.state.city} name="city" onChange={(e) => this.setState({ [e.target.name]: e.target.value }, console.log(this.state.city))} />
                        </div>
                        <div>
                            <input value={this.state.tittle} name="tittle" onChange={(e) => this.setState({ [e.target.name]: e.target.value }, console.log(this.state.tittle))} />
                        </div>
                        <div>
                            <input value={this.state.deadline} name="deadline" onChange={(e) => this.setState({ [e.target.name]: e.target.value }, console.log(this.state.deadline))} />
                        </div>
                        <div>
                            <input value={this.state.money} name="money" onChange={(e) => this.setState({ [e.target.name]: e.target.value }, console.log(this.state.money))} />
                        </div>
                        <button className="taskForm_Button" onClick={this.handleAddTask}>Add</button> 
                    </div>
                </div>
            </div>
        )
    }
}

export default TaskForm;