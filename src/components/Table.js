import React from 'react';

class Table extends React.Component {
    render() {
        return (
            <div>
                <div className='table_header'>
                    <div className='ID'>ID</div>
                    <div className='city'>Город</div>
                    <div className='tittle'>Название</div>
                    <div className='deadline'>Выполнить до</div>
                    <div className='money'>Сумма</div>
                </div>
            {this.props.filtredItems.map(el => 
                <div key={el.id} className="tasks_list">
                <div>{el.ID}</div>
                <div>{el.city}</div>
                <div>{el.tittle}</div>
                <div>{el.deadline}</div>
                <div>{el.money}</div>
                </div>
            )}
            </div>
        )
    }
}

export default Table;