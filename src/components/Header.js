import React from "react";
import { NavLink } from 'react-router-dom'
import '../styles/App.css';
const links = [
{ tittle: 'Задания', to: '/tasks' },
{ tittle: 'Финансы', to: '/finance' },
{ tittle: 'Компания', to: '/company' },
{ tittle: 'Статистика', to: '/stats' }
]
const Header = () => {
    return (
        <div className="header">
            <div className="header_left">
                <div className="logo">taskon</div>
                {links.map(link => <NavLink to={link.to} activeClassName='active' className='header_task'>{link.tittle}</NavLink>)}
            </div>
            <div className="header_right">
                <div>Баланс: 218 982,32</div>
                <select className="header_select"><option>User</option></select>
                <img src={"src/images/ic-notifications-black-24-px.png"} />
                <div>
                    <img src={"src/images/ic-help-outline-24-px-copy-3@2x.png"} className="question_mark" />
                </div>
            </div>
        </div>
    );
}

export default Header;