import React, { useState } from 'react';
import Inwork from '../Tasks/InWork.js';
import Considering from '../Tasks/Considering';
import Done from '../Tasks/Done';
import Canceled from '../Tasks/Canceled';
import Scatching from '../Tasks/Scatching';
import NewTasks from '../Tasks/NewTasks';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink,
    useRouteMatch
  } from "react-router-dom";

const newTasks = () => {
    let { path, url } = useRouteMatch();

    return (
        <div className="tasks">
            <div className="tasks_name">
                Задания
            </div>
            <Router>
                <div className="tabs">
                    <div><NavLink to={`${url}/new`}  className="header__tasks" exact activeClassName="header__tasks_active">Новые</NavLink></div>
                    <div><NavLink to={`${url}/inwork`}  className="header__tasks" exact activeClassName="header__tasks_active">В работе</NavLink></div>
                    <div><NavLink to={`${url}/considering`} className="header__tasks" exact activeClassName="header__tasks_active">На рассмотрении</NavLink></div>
                    <div><NavLink to={`${url}/done`} className="header__tasks" exact activeClassName="header__tasks_active">Выполненны</NavLink></div>
                    <div><NavLink to={`${url}/canceled`} className="header__tasks" exact activeClassName="header__tasks_active">Отменены</NavLink></div>
                    <div><NavLink to={`${url}/scatching`} className="header__tasks" exact activeClassName="header__tasks_active">Черновики</NavLink></div>
                </div>
                <div className="tabs_content">
                <Switch>
                    <Route path={path}>
                        <NewTasks />
                    </Route>
                    <Route path={`${path}/done`}>
                        <Done />
                    </Route>
                    <Route path={`${path}/inwork`}>
                        <Inwork />
                    </Route>
                    <Route path={`${path}/considering`}>
                        <Considering />
                    </Route>
                    <Route path={`${path}/scatching`}>
                        <Scatching />
                    </Route>
                    <Route path={`${path}/canceled`}>
                        <Canceled />
                    </Route>
                </Switch>
                </div>
            </Router>
        </div>
    )
}


export default newTasks;